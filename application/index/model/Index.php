<?php
namespace app\index\model;
use app\common\model\Base;
use think\Db;
class Index extends Base
{
    //查询
    public function getUser($where,$limit) {

        $res =  Db::name('user')
                ->where($where)
                ->limit($limit)
                //->fetchSql();
                ->select();
        return $res;
    }


    //分页总数
    public function pageCount($where) {
        $count = Db::name('user')
                    ->where("$where")
                    ->count();
        return $count;
    }
}