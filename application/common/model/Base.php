<?php
namespace app\common\model;
use think\Model;

class Base extends Model
{
    protected $resultSetType = 'collection';
    
    public function initialize()
    {
    	parent::initialize();
    }
}