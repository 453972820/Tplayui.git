<?php
namespace app\common\controller;

use think\Controller;
use think\Db;


class Base extends Controller
{

    public function _initialize()
    {
        parent::_initialize();
        header("Content-type:text/html;charset=utf-8");

    }

    //pc、mobile 路由总入口
    public function index()
    {

        //if (computer_or_mobile() == 'computer') $this->redirect('index/Index/home');
        //if (computer_or_mobile() == 'mobile') $this->redirect('mobile/index/mhome');
        echo '1212';
    }



    /**
     * @Purpose     验证码生成
     */
    public function verify()
    {
        ob_clean();
        $captcha = new Captcha();
        $captcha->imageH = 0;
        $captcha->imageW = 0;
        $captcha->length = 4;
        $captcha->codeSet = '0123456789';
        $captcha->fontSize = 25;
        $captcha->useCurve = false;
        $captcha->useNoise = false;
        return $captcha->entry();
    }

}
