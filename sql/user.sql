/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50553
Source Host           : 127.0.0.1:3306
Source Database       : print

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2020-01-12 23:44:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `experience` varchar(50) DEFAULT NULL COMMENT '积分',
  `sex` varchar(50) DEFAULT NULL,
  `score` varchar(100) DEFAULT NULL COMMENT '评分',
  `city` varchar(50) DEFAULT NULL,
  `sign` varchar(50) DEFAULT NULL COMMENT '签名',
  `classify` varchar(255) DEFAULT NULL COMMENT '职业',
  `wealth` varchar(50) DEFAULT NULL COMMENT '发证日期',
  `crtime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uptime` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0删除1存在',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=548 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('2', '赵晓伟', '884', '男', '27', '北京', '签名-1', '词人', '82830700', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('3', '程晋虹', '650', '女', '31', '上海', '签名-2', '酱油', '64928690', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('4', '车为民', '362', '女', '68', '石家庄', '签名-3', '诗人', '6298078', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('5', '柴志强', '807', '男', '6', '太原', '签名-4', '作家', '37117017', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('6', '王建国', '173', '女', '87', '成都', '签名-5', '作家', '76263262', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('7', '张玉生', '982', '女', '34', '武汉', '签名-6', '作家', '60344147', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('8', '安静', '727', '男', '28', '四川', '签名-7', '作家', '57768166', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('9', '周太生', '951', '男', '14', '北京', '签名-8', '词人', '82030578', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('10', '孟志国', '484', '女', '75', '北京', '签名-9', '词人', '16503371', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('11', '岳旭明', '278482', '女', '57', '上海', '签名-10', '词人', '86801934', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('12', '秦福军', '278504', '男', '27', '石家庄', '签名-11', '作家', '578', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('13', '林树东', '278720', '女', '31', '太原', '签名-12', '词人', '562695', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('14', '朱云贵', '398209', '女', '68', '成都', '签名-13', '酱油', '54710', '2020-01-11 16:18:10', null, '0');
INSERT INTO `user` VALUES ('547', '卢跃同', '255', '女', '57', '北京', '签名-0', '作家', '531514', '2020-01-11 16:18:10', null, '0');
